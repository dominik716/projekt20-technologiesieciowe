﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Client
{
    public partial class Klient : Form
    {
        public Klient()
        {
            InitializeComponent();
            FormClosing += new FormClosingEventHandler(Form1_Closing);
            Client.messagesListView = messagesListView;
            Client.resultLabel = resultLabel;
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void disconnect_Click(object sender, EventArgs e)
        {
            Client.Disconnect();
        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void connect_Click(object sender, EventArgs e)
        {
            disconnect.Enabled = true;
            string ip = adressIP.Text;
            int port = int.Parse(portTextBox.Text);
            connect.Enabled = false;
            Client.Connect(ip, port);
            mySessionIdentifier.Text = "Mój Identyfikator sesji: " + Client.mySessionIdentifier;
            statusLabel.Text = Client.statusStringInfo;
            //updateListView();
        }

        private void adressIP_TextChanged(object sender, EventArgs e)
        {

        }

        private void sendMessageTextbox_TextChanged(object sender, EventArgs e)
        {

        }

        private void sendButton_Click(object sender, EventArgs e)
        {
            ListViewItem row = new ListViewItem(Client.getUnixTimestamp());
            row.SubItems.Add("--->");
            row.SubItems.Add(sendMessageTextbox.Text);
            messagesListView.Items.Add(row);

            Client.SendMessage(sendMessageTextbox.Text);
            //updateListView();

        }

        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void Form1_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (MessageBox.Show("Na pewno chcesz wyjść?", "Ostrzeżenie", MessageBoxButtons.YesNo) == DialogResult.No)
            {
                e.Cancel = true;
                
            }
            else
            {
                Client.Disconnect();
            }
            
        }

        private void addButton_Click(object sender, EventArgs e)
        {
            Client.SendCalculation("op3", number1Textbox.Text, number2Textbox.Text, number3Textbox.Text);
        }

        private void divideButton_Click(object sender, EventArgs e)
        {
            Client.SendCalculation("op2", number1Textbox.Text, number2Textbox.Text, number3Textbox.Text);
        }

        private void multiplyButton_Click(object sender, EventArgs e)
        {
            Client.SendCalculation("op4", number1Textbox.Text, number2Textbox.Text, number3Textbox.Text);

        }

        private void subtractButton_Click(object sender, EventArgs e)
        {
            Client.SendCalculation("op1", number1Textbox.Text, number2Textbox.Text, number3Textbox.Text);
        }

        /*private void updateListView()
        {
            ListViewItem row;
            while (Client.MessagesQueue.Count > 0)
            {
                Dictionary<string, string> rowFromQueue = Client.MessagesQueue.Dequeue();
                row = new ListViewItem(rowFromQueue["time"]);
                row.SubItems.Add(rowFromQueue["direction"]);
                row.SubItems.Add(rowFromQueue["message"]);
                messagesListView.Items.Add(row);
            }
        }*/
    }
}
