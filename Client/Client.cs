﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Collections;
using System.Net;
using System.Windows.Forms;

namespace Client
{
    public static class Client
    {
        public static string mySessionIdentifier;
        public static int port;
        public static TcpClient client;
        public static NetworkStream stream;
        public static string statusStringInfo;
        public static string Operation;

        public static ListView messagesListView;
        public static Label resultLabel;


        public static void Connect(String server, int port)
        {
            client = new TcpClient(server, port);
            stream = client.GetStream();
            SendMessage("OperaC>>getID^");
            statusStringInfo = client.Connected ? "Połączono" : "Rozłączono"; //Winformsy aktualizacja statusu
        }

        public static void SendCalculation(string operation, string number1 = "", string number2 = "", string number3 = "")
        {
            ListViewItem row;
            row = new ListViewItem(getUnixTimestamp());
            row.SubItems.Add("--->");
            row.SubItems.Add("Operacja:" + operation + ", liczby:" + number1 + "," + number2 + "," + number3);
            messagesListView.Items.Add(row);
            var command = new StringBuilder();



            if (!int.TryParse(number1, out int n) || !int.TryParse(number2, out int n2) || !int.TryParse(number3, out int n3)) //jeśli zmienne nie zawierają liczb
            {
                MessageBox.Show("Nie podano liczb");
            }
            else
            {
                command.Append("OperaC>>" + operation + "^");
                command.Append("numbe1>>" + number1 + "^");
                command.Append("numbe2>>" + number2 + "^");
                command.Append("numbe3>>" + number3 + "^");
                SendMessage(command.ToString());

            }

        }

        public static void SendMessage(String message)
        {
            var builder = new StringBuilder();
            if(!string.IsNullOrEmpty(mySessionIdentifier)) builder.Append("IdentY>>" + mySessionIdentifier + "^");
            builder.Append("CurrTi>>" + getUnixTimestamp() + "^");
            builder.Append("StatuS>>" + "OK" + "^");
            if(!message.Contains("OperaC>>"))builder.Append("OperaC>>" + "none" + "^");
            builder.Append(message);

            Byte[] data = System.Text.Encoding.ASCII.GetBytes(builder.ToString());
            
            stream.Write(data, 0, data.Length);     // Wysłanie wiadomości do serwera.  
            Console.WriteLine("Wyslano: {0}", message);
            String response = String.Empty;
            
            Byte[] dataResponse = new Byte[512];
            Int32 bytes = stream.Read(dataResponse, 0, dataResponse.Length);    // Odczytanie odpowiedzi z serwera
            response = System.Text.Encoding.ASCII.GetString(dataResponse, 0, bytes);
            DecodeResponse(response);
            Console.WriteLine("Odpowiedz: {0}", response);
            //stream.Close();
        }

        public static void DecodeResponse(String message)
        {
            var messageList = message.Split(new[] { "^" }, StringSplitOptions.None);      
            foreach (string OneMessage in messageList)
            {
                if (String.IsNullOrEmpty(OneMessage)) continue;
                string[] OneMessageSplitted = OneMessage.Split(new[] { ">>" }, StringSplitOptions.None);

                if (OneMessageSplitted[0].Equals("IdentY"))
                {
                    mySessionIdentifier = OneMessageSplitted[1];
                    continue;
                }
                else if (OneMessageSplitted[0].Equals("test"))
                {
                    Console.WriteLine("Znalazlem naglowek  o tresci test. Jego zawartosc: " + OneMessageSplitted[1]);
                    continue;
                }
                else if (OneMessageSplitted[0].Equals("CurrTi"))
                {
                    continue;
                }
                else if (OneMessageSplitted[0].Equals("StatuS"))
                {
                    Console.WriteLine(OneMessageSplitted);
                    if (OneMessageSplitted.Length > 1 && OneMessageSplitted[1].Contains("Error:") && OneMessageSplitted[1].Substring(0, 6) == "Error:")
                    {
                        if (OneMessageSplitted[1].Substring(6) == "1")
                        {
                            addToMessageList("Wystąpił błąd:" + "Proba dzielenia przez zero");
                            MessageBox.Show("Wystąpił błąd:" + "Proba dzielenia przez zero");
                        }

                    }
                    continue;
                }
                else if (OneMessageSplitted[0].Equals("OperaC"))
                {
                    continue;
                }
                else if(OneMessageSplitted[0].Equals("messag"))
                {
                    Console.WriteLine("Otrzymałem wiadomosc: " + OneMessageSplitted[1]);
                    addToMessageList(OneMessageSplitted[0] + ": " + OneMessageSplitted[1]);
                    continue;
                }
                else if (OneMessageSplitted[0].Equals("pinggg"))
                {
                    Console.WriteLine("Ping z serwera: " + OneMessageSplitted[1]);
                    addToMessageList(OneMessageSplitted[0] + ": " + OneMessageSplitted[1]);
                    continue;
                }
                else if (OneMessageSplitted[0].Equals("wynikk"))
                {
                    Console.WriteLine("Wynik z serwera: " + OneMessageSplitted[1]);
                    addToMessageList("Wynik Operacji: " + OneMessageSplitted[1]);
                    resultLabel.Text = OneMessageSplitted[1];
                    continue;
                }
                else
                {
                    addToMessageList(String.Join(":", OneMessageSplitted));
                }
            }
        }

        public static void ConnectOld(String server, String message)
        {
            try
            {
                Int32 port = 13000;
                TcpClient client = new TcpClient(server, port);
                NetworkStream stream = client.GetStream();
                int count = 0;
                while (count++ < 3)
                {
                    // Konwersja wiadomości na ASCII.
                    Byte[] data = System.Text.Encoding.ASCII.GetBytes(message);
                    // Wysłanie wiadomości. 
                    stream.Write(data, 0, data.Length);
                    Console.WriteLine("Sent: {0}", message);
                    // Zmienna na odpowiedź z serwera.
                    data = new Byte[512];
                    String response = String.Empty;
                    // Odczytaj odpowiedź z serwera.
                    Int32 bytes = stream.Read(data, 0, data.Length);
                    response = System.Text.Encoding.ASCII.GetString(data, 0, bytes);
                    Console.WriteLine("Otrzymano: {0}", response);
                    Thread.Sleep(2000);
                }
                stream.Close();
                client.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception: {0}", e);
            }
            Console.Read();
        }

        public static void addToMessageList(string message)
        {
            ListViewItem row;

            row = new ListViewItem(getUnixTimestamp());
            row.SubItems.Add("<---");
            row.SubItems.Add(message);
            messagesListView.Items.Add(row);

            /*var row = new Dictionary<string, string>();
            row["time"] = getUnixTimestamp();
            row["direction"] = "<---";
            row["message"] = message;
            MessagesQueue.Enqueue(row);*/
        }

        public static void Disconnect()
        {
            if(client != null && client.Connected)
            {
                Console.WriteLine("Disconnect");
                stream.Close();
                client.Close();
            }
        }

        public static string GetLocalIPAddress()
        {
            var host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (var ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    return ip.ToString();
                }
            }
            throw new Exception("Problem z pobraniem lokalnego adresu IP!");
        }

        public static string getUnixTimestamp()
        {
            return ((Int32)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds).ToString();
        }

    }
}
