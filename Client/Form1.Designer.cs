﻿namespace Client
{
    partial class Klient
    {
        /// <summary>
        /// Wymagana zmienna projektanta.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Wyczyść wszystkie używane zasoby.
        /// </summary>
        /// <param name="disposing">prawda, jeżeli zarządzane zasoby powinny zostać zlikwidowane; Fałsz w przeciwnym wypadku.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kod generowany przez Projektanta formularzy systemu Windows

        /// <summary>
        /// Metoda wymagana do obsługi projektanta — nie należy modyfikować
        /// jej zawartości w edytorze kodu.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Klient));
            this.adressIP = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.portTextBox = new System.Windows.Forms.TextBox();
            this.connect = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.statusLabel = new System.Windows.Forms.Label();
            this.disconnect = new System.Windows.Forms.Button();
            this.messagesListView = new System.Windows.Forms.ListView();
            this.time = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.direction = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.message = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.sendMessageTextbox = new System.Windows.Forms.TextBox();
            this.sendButton = new System.Windows.Forms.Button();
            this.mySessionIdentifier = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.resultLabel = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.divideButton = new System.Windows.Forms.Button();
            this.multiplyButton = new System.Windows.Forms.Button();
            this.subtractButton = new System.Windows.Forms.Button();
            this.addButton = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.number3Textbox = new System.Windows.Forms.TextBox();
            this.number2Textbox = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.number1Textbox = new System.Windows.Forms.TextBox();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // adressIP
            // 
            this.adressIP.Location = new System.Drawing.Point(34, 41);
            this.adressIP.Name = "adressIP";
            this.adressIP.Size = new System.Drawing.Size(194, 22);
            this.adressIP.TabIndex = 0;
            this.adressIP.Text = "192.168.0.12";
            this.adressIP.TextChanged += new System.EventHandler(this.adressIP_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(31, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(115, 17);
            this.label1.TabIndex = 1;
            this.label1.Text = "Adres IP serwera";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(293, 21);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(34, 17);
            this.label2.TabIndex = 2;
            this.label2.Text = "Port";
            // 
            // portTextBox
            // 
            this.portTextBox.Location = new System.Drawing.Point(296, 41);
            this.portTextBox.Name = "portTextBox";
            this.portTextBox.Size = new System.Drawing.Size(100, 22);
            this.portTextBox.TabIndex = 3;
            this.portTextBox.Text = "13000";
            // 
            // connect
            // 
            this.connect.Location = new System.Drawing.Point(425, 21);
            this.connect.Name = "connect";
            this.connect.Size = new System.Drawing.Size(89, 42);
            this.connect.TabIndex = 4;
            this.connect.Text = "Połącz";
            this.connect.UseVisualStyleBackColor = true;
            this.connect.Click += new System.EventHandler(this.connect_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(31, 76);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(124, 17);
            this.label3.TabIndex = 5;
            this.label3.Text = "Status połączenia:";
            // 
            // statusLabel
            // 
            this.statusLabel.AutoSize = true;
            this.statusLabel.Location = new System.Drawing.Point(161, 76);
            this.statusLabel.Name = "statusLabel";
            this.statusLabel.Size = new System.Drawing.Size(81, 17);
            this.statusLabel.TabIndex = 6;
            this.statusLabel.Text = "Rozłączony";
            // 
            // disconnect
            // 
            this.disconnect.Enabled = false;
            this.disconnect.ForeColor = System.Drawing.SystemColors.ControlText;
            this.disconnect.Location = new System.Drawing.Point(529, 21);
            this.disconnect.Name = "disconnect";
            this.disconnect.Size = new System.Drawing.Size(93, 42);
            this.disconnect.TabIndex = 7;
            this.disconnect.Text = "Rozłącz";
            this.disconnect.UseVisualStyleBackColor = true;
            this.disconnect.Click += new System.EventHandler(this.disconnect_Click);
            // 
            // messagesListView
            // 
            this.messagesListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.time,
            this.direction,
            this.message});
            this.messagesListView.HideSelection = false;
            this.messagesListView.Location = new System.Drawing.Point(34, 109);
            this.messagesListView.Name = "messagesListView";
            this.messagesListView.Size = new System.Drawing.Size(588, 196);
            this.messagesListView.TabIndex = 8;
            this.messagesListView.UseCompatibleStateImageBehavior = false;
            this.messagesListView.View = System.Windows.Forms.View.Details;
            this.messagesListView.SelectedIndexChanged += new System.EventHandler(this.listView1_SelectedIndexChanged);
            // 
            // time
            // 
            this.time.Text = "Czas";
            this.time.Width = 71;
            // 
            // direction
            // 
            this.direction.Text = "Kierunek";
            this.direction.Width = 79;
            // 
            // message
            // 
            this.message.Text = "Wiadomość";
            this.message.Width = 223;
            // 
            // sendMessageTextbox
            // 
            this.sendMessageTextbox.Location = new System.Drawing.Point(34, 318);
            this.sendMessageTextbox.Name = "sendMessageTextbox";
            this.sendMessageTextbox.Size = new System.Drawing.Size(507, 22);
            this.sendMessageTextbox.TabIndex = 9;
            this.sendMessageTextbox.TextChanged += new System.EventHandler(this.sendMessageTextbox_TextChanged);
            // 
            // sendButton
            // 
            this.sendButton.Location = new System.Drawing.Point(547, 311);
            this.sendButton.Name = "sendButton";
            this.sendButton.Size = new System.Drawing.Size(75, 36);
            this.sendButton.TabIndex = 10;
            this.sendButton.Text = "Wyślij";
            this.sendButton.UseVisualStyleBackColor = true;
            this.sendButton.Click += new System.EventHandler(this.sendButton_Click);
            // 
            // mySessionIdentifier
            // 
            this.mySessionIdentifier.AutoSize = true;
            this.mySessionIdentifier.Location = new System.Drawing.Point(327, 76);
            this.mySessionIdentifier.Name = "mySessionIdentifier";
            this.mySessionIdentifier.Size = new System.Drawing.Size(89, 17);
            this.mySessionIdentifier.TabIndex = 11;
            this.mySessionIdentifier.Text = "Identyfikator:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(31, 349);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(272, 17);
            this.label4.TabIndex = 12;
            this.label4.Text = "Made By: Dominik Adamek, Maciej Chajda";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.resultLabel);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.divideButton);
            this.groupBox1.Controls.Add(this.multiplyButton);
            this.groupBox1.Controls.Add(this.subtractButton);
            this.groupBox1.Controls.Add(this.addButton);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.number3Textbox);
            this.groupBox1.Controls.Add(this.number2Textbox);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.number1Textbox);
            this.groupBox1.Location = new System.Drawing.Point(42, 409);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(585, 214);
            this.groupBox1.TabIndex = 13;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Operacje matematyczne";
            // 
            // resultLabel
            // 
            this.resultLabel.AutoSize = true;
            this.resultLabel.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.resultLabel.Location = new System.Drawing.Point(249, 171);
            this.resultLabel.Name = "resultLabel";
            this.resultLabel.Size = new System.Drawing.Size(52, 28);
            this.resultLabel.TabIndex = 11;
            this.resultLabel.Text = "_____";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(185, 171);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(76, 28);
            this.label8.TabIndex = 10;
            this.label8.Text = "Wynik: ";
            // 
            // divideButton
            // 
            this.divideButton.Location = new System.Drawing.Point(166, 112);
            this.divideButton.Name = "divideButton";
            this.divideButton.Size = new System.Drawing.Size(106, 43);
            this.divideButton.TabIndex = 9;
            this.divideButton.Text = "Podziel";
            this.divideButton.UseVisualStyleBackColor = true;
            this.divideButton.Click += new System.EventHandler(this.divideButton_Click);
            // 
            // multiplyButton
            // 
            this.multiplyButton.Location = new System.Drawing.Point(288, 112);
            this.multiplyButton.Name = "multiplyButton";
            this.multiplyButton.Size = new System.Drawing.Size(106, 43);
            this.multiplyButton.TabIndex = 8;
            this.multiplyButton.Text = "Pomnóż";
            this.multiplyButton.UseVisualStyleBackColor = true;
            this.multiplyButton.Click += new System.EventHandler(this.multiplyButton_Click);
            // 
            // subtractButton
            // 
            this.subtractButton.Location = new System.Drawing.Point(410, 112);
            this.subtractButton.Name = "subtractButton";
            this.subtractButton.Size = new System.Drawing.Size(106, 43);
            this.subtractButton.TabIndex = 7;
            this.subtractButton.Text = "Odejmij";
            this.subtractButton.UseVisualStyleBackColor = true;
            this.subtractButton.Click += new System.EventHandler(this.subtractButton_Click);
            // 
            // addButton
            // 
            this.addButton.Location = new System.Drawing.Point(41, 112);
            this.addButton.Name = "addButton";
            this.addButton.Size = new System.Drawing.Size(106, 43);
            this.addButton.TabIndex = 6;
            this.addButton.Text = "Dodaj";
            this.addButton.UseVisualStyleBackColor = true;
            this.addButton.Click += new System.EventHandler(this.addButton_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(369, 37);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(61, 17);
            this.label7.TabIndex = 5;
            this.label7.Text = "Liczba 3";
            // 
            // number3Textbox
            // 
            this.number3Textbox.Location = new System.Drawing.Point(372, 60);
            this.number3Textbox.Name = "number3Textbox";
            this.number3Textbox.Size = new System.Drawing.Size(100, 22);
            this.number3Textbox.TabIndex = 4;
            // 
            // number2Textbox
            // 
            this.number2Textbox.Location = new System.Drawing.Point(203, 60);
            this.number2Textbox.Name = "number2Textbox";
            this.number2Textbox.Size = new System.Drawing.Size(100, 22);
            this.number2Textbox.TabIndex = 3;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(200, 37);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(61, 17);
            this.label6.TabIndex = 2;
            this.label6.Text = "Liczba 2";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(38, 37);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(61, 17);
            this.label5.TabIndex = 1;
            this.label5.Text = "Liczba 1";
            // 
            // number1Textbox
            // 
            this.number1Textbox.Location = new System.Drawing.Point(38, 60);
            this.number1Textbox.Name = "number1Textbox";
            this.number1Textbox.Size = new System.Drawing.Size(100, 22);
            this.number1Textbox.TabIndex = 0;
            // 
            // Klient
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(662, 652);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.mySessionIdentifier);
            this.Controls.Add(this.sendButton);
            this.Controls.Add(this.sendMessageTextbox);
            this.Controls.Add(this.messagesListView);
            this.Controls.Add(this.disconnect);
            this.Controls.Add(this.statusLabel);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.connect);
            this.Controls.Add(this.portTextBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.adressIP);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Klient";
            this.Text = "Klient";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox adressIP;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox portTextBox;
        private System.Windows.Forms.Button connect;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label statusLabel;
        private System.Windows.Forms.Button disconnect;
        private System.Windows.Forms.ListView messagesListView;
        private System.Windows.Forms.ColumnHeader time;
        private System.Windows.Forms.ColumnHeader direction;
        private System.Windows.Forms.ColumnHeader message;
        private System.Windows.Forms.TextBox sendMessageTextbox;
        private System.Windows.Forms.Button sendButton;
        private System.Windows.Forms.Label mySessionIdentifier;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button divideButton;
        private System.Windows.Forms.Button multiplyButton;
        private System.Windows.Forms.Button subtractButton;
        private System.Windows.Forms.Button addButton;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox number3Textbox;
        private System.Windows.Forms.TextBox number2Textbox;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox number1Textbox;
        private System.Windows.Forms.Label resultLabel;
    }
}

