﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace Server
{
    public static class Server
    {
        static TcpListener server = null;

        //Kontrolki z winforms
        public static ListView messagesListView;
        public static ListView userListView;

        public static Dictionary<string, string> IPAddresses = new Dictionary<string, string>();    // sesja <-> ipadress
        public static Dictionary<string, string> UserStatuses = new Dictionary<string, string>();    // sesja <-> status

        public static Dictionary<string, string> commandsDictionary = new Dictionary<string, string>();    // command <-> value






        public static void Start(string ip, int port)
        {
            try
            {
                IPAddress localAddr = IPAddress.Parse(ip);
                server = new TcpListener(localAddr, port);  //uruchomienie nasłuchu na danym porcie
                server.Start();
                StartListener();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString());
            }

        }

        public static void StartListener()
        {
            try
            {
                while (true)
                {
                    Console.WriteLine("Oczekiwanie na połączenie...");
                    TcpClient client = server.AcceptTcpClient();
                    Console.WriteLine("Połączono!");

                    Thread t = new Thread(new ParameterizedThreadStart(HandleDevice));  //przypisanie nowego wątku do połączonego klienta 
                    t.Start(client);
                }
            }
            catch (SocketException e)
            {
                Console.WriteLine("SocketException: {0}", e);
                MessageBox.Show(e.ToString());
                server.Stop();
            }
        }


        public static void HandleDevice(Object obj)
        {
            string sessionIdentifier = "";
            if (String.IsNullOrEmpty(sessionIdentifier)) sessionIdentifier = getSessionIdentifier();

            TcpClient client = (TcpClient)obj;
            var stream = client.GetStream();
            string imei = String.Empty;
            IPAddresses[sessionIdentifier] = ((IPEndPoint)client.Client.RemoteEndPoint).Address.ToString();

            UserStatuses[sessionIdentifier] = "Połączono";

            string data = null;
            Byte[] bytes = new Byte[512];
            int i;
            try
            {
                while ((i = stream.Read(bytes, 0, bytes.Length)) != 0)   //odczyt danych ze strumienia
                {
                    string hex = BitConverter.ToString(bytes);
                    data = Encoding.ASCII.GetString(bytes, 0, i);
                    DecodeResponse(client, sessionIdentifier, data);        //odczyt odpowiedzi
                    Console.WriteLine("{1}: Received: {0}", data, Thread.CurrentThread.ManagedThreadId);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Uzytkownik się rozłączył");
                Console.WriteLine("Exception: {0}", e.ToString());
                client.Close();
            }
            UserStatuses[sessionIdentifier] = "Rozłączono";
            updateUserList();
            Console.WriteLine("Doszedlem do konca");
        }

        public static void SendReply(TcpClient client, String sessionIdentifier, String message)
        {
            updateUserList();

            var stream = client.GetStream();
            //var builder = new StringBuilder();
            Byte[] data = System.Text.Encoding.ASCII.GetBytes(message.ToString());
            stream.Write(data, 0, data.Length);                     //wysłanie odpowiedzi

            //ostatnia czesckomendy
            var commands = message.Split(new[] { "^" }, StringSplitOptions.None);
            commands = commands.Where(x => !string.IsNullOrEmpty(x)).ToArray(); // usuwa puste Elementy
            string command = commands[commands.Length - 1];
            addToMessageList(sessionIdentifier, command, "--->");
        }

        public static void DecodeResponse(TcpClient client, String sessionIdentifier, String message)
        {
            var stream = client.GetStream();
            var builder = new StringBuilder();
            builder.Append("CurrTi>>" + getUnixTimestamp() + "^");
            builder.Append("IdentY>>" + sessionIdentifier + "^");

            Console.WriteLine("Wiadomosc: " + message);
            var messageList = message.Split(new[] { "^" }, StringSplitOptions.None);

            createCommandDictionaryFromString(message);

            //nowe
            if (commandsDictionary.ContainsKey("pinggg"))
            {
                builder.Append("ponggg>>" + "^");
                addToMessageList(sessionIdentifier, "pinggg", "<---");
                SendReply(client, sessionIdentifier, builder.ToString());
            }

            if (commandsDictionary.ContainsKey("OperaC"))
            {
                if (commandsDictionary["OperaC"] == "getID")
                {
                    builder.Append("StatuS>>OK" + "^");
                    addToMessageList(sessionIdentifier, "getID:" + sessionIdentifier, "<---");
                    builder.Append("OperaC>>getID" + "^");
                    SendReply(client, sessionIdentifier, builder.ToString());
                }
                else if (commandsDictionary["OperaC"] == "none" || string.IsNullOrEmpty(commandsDictionary["OperaC"]))
                {

                }
                else
                {
                    string wynik = "";
                    try {
                        wynik = calculateString(commandsDictionary["OperaC"], commandsDictionary["numbe1"], commandsDictionary["numbe2"], commandsDictionary["numbe3"]);
                        builder.Append("OperaC>>" + commandsDictionary["OperaC"] + "^");
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.Message);
                        builder.Append("StatuS>>Error:" + 1 +  "^");
                        builder.Append("OperaC>>" + commandsDictionary["OperaC"] + "^");
                        SendReply(client, sessionIdentifier, builder.ToString());
                        return;
                    }
                    builder.Append("wynikk>>" + wynik + "^");
                    builder.Append("StatuS>>OK" + "^");
                    addToMessageList(sessionIdentifier, "Operacja:" + commandsDictionary["OperaC"] + ", liczby:" + commandsDictionary["numbe1"] + "," + commandsDictionary["numbe2"] + "," + commandsDictionary["numbe3"], "<---");
                    SendReply(client, sessionIdentifier, builder.ToString());

                }
            }
            if (commandsDictionary.ContainsKey("messag"))
            {
                builder.Append("StatuS>>OK" + "^");
                builder.Append("messag>>" + commandsDictionary["messag"] + "^");
                addToMessageList(sessionIdentifier, "messag:" + commandsDictionary["messag"], "<---");
                SendReply(client, sessionIdentifier, builder.ToString());
            }
        }


        public static void addToMessageList(string sessionIdentifier, string message, string direction)
        {
            ListViewItem row2 = new ListViewItem(getUnixTimestamp());
            row2.SubItems.Add(sessionIdentifier);
            row2.SubItems.Add(direction);
            row2.SubItems.Add(message);
            messagesListView.Items.Add(row2);
        }

        public static Dictionary<string, string> createCommandDictionaryFromString(string messageFromClient)
        {
            commandsDictionary = new Dictionary<string, string>();
            var messageList = messageFromClient.Split(new[] { "^" }, StringSplitOptions.None);
            foreach (string OneMessage in messageList)
            {
                if (String.IsNullOrEmpty(OneMessage)) continue;
                string[] OneMessageSplitted = OneMessage.Split(new[] { ">>" }, StringSplitOptions.None);
                Console.WriteLine(OneMessageSplitted.ToString());

                if (OneMessageSplitted.ElementAtOrDefault(1) == null)
                {
                    commandsDictionary[OneMessageSplitted[0]] = "";
                }
                else
                {
                    commandsDictionary.Add(OneMessageSplitted[0], OneMessageSplitted[1]);
                }
            }
            return commandsDictionary;
        }

        public static string calculateString(string operation, string number1, string number2, string number3)
        {
            if (!int.TryParse(number1, out int n) || !int.TryParse(number2, out int n2) || !int.TryParse(number3, out int n3)) //jeśli zmienne nie zawierają liczb
            {
                throw new Exception("Nie podano liczb");
            }


            float wynik = 0;

            if (operation == "op3")
            {
                wynik = float.Parse(number1) + float.Parse(number2) + float.Parse(number3);
            }
            else if (operation == "op1")
            {
                wynik = float.Parse(number1) - float.Parse(number2) - float.Parse(number3);
            }
            else if (operation == "op4")
            {
                wynik = float.Parse(number1) * float.Parse(number2) * float.Parse(number3);
            }
            else if (operation == "op2")
            {
                if (float.Parse(number2) == 0 || float.Parse(number3) == 0) throw new DivideByZeroException();
                wynik = float.Parse(number1) / float.Parse(number2) / float.Parse(number3);
            }
            /* else if (operation == "power")
             {
                 return Math.Pow(int.Parse(arguments[i]), int.Parse(arguments[i + 1])).ToString();
             }*/
            if (float.IsInfinity(wynik) || float.IsNaN(wynik)) throw new Exception("Result not a number");
            return wynik.ToString();
        }


        public static string getSessionIdentifier()
        {
            Random rnd = new Random();
            string id = rnd.Next(10000, 99999).ToString();
            while (IPAddresses.ContainsKey(id))
            {
                id = rnd.Next(10000, 99999).ToString();
            }
            return id;
        }

        public static string GetLocalIPAddress()
        {
            var host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (var ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    return ip.ToString();
                }
            }
            throw new Exception("Problem z pobraniem lokalnego adresu IP");
        }

        public static string getUnixTimestamp()
        {
            return ((Int32)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds).ToString();
        }


        public static void updateUserList()
        {
            userListView.Items.Clear();
            foreach (string key in IPAddresses.Keys.ToArray())
            {
                ListViewItem row = new ListViewItem(IPAddresses[key]);
                row.SubItems.Add(key);
                row.SubItems.Add(UserStatuses[key]);
                userListView.Items.Add(row);
            }

        }


    }
}
