﻿namespace Server
{
    partial class Form1
    {
        /// <summary>
        /// Wymagana zmienna projektanta.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Wyczyść wszystkie używane zasoby.
        /// </summary>
        /// <param name="disposing">prawda, jeżeli zarządzane zasoby powinny zostać zlikwidowane; Fałsz w przeciwnym wypadku.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kod generowany przez Projektanta formularzy systemu Windows

        /// <summary>
        /// Metoda wymagana do obsługi projektanta — nie należy modyfikować
        /// jej zawartości w edytorze kodu.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.myLocalIP = new System.Windows.Forms.Label();
            this.usersListView = new System.Windows.Forms.ListView();
            this.adresIP = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.sessionID = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.status = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.portTextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.startServerButton = new System.Windows.Forms.Button();
            this.messagesListView = new System.Windows.Forms.ListView();
            this.time = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.sessionIdentifier = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.direction = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.message = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // myLocalIP
            // 
            this.myLocalIP.AutoSize = true;
            this.myLocalIP.Location = new System.Drawing.Point(30, 22);
            this.myLocalIP.Name = "myLocalIP";
            this.myLocalIP.Size = new System.Drawing.Size(138, 17);
            this.myLocalIP.TabIndex = 0;
            this.myLocalIP.Text = "Mój lokalny adres IP:";
            this.myLocalIP.Click += new System.EventHandler(this.label1_Click);
            // 
            // usersListView
            // 
            this.usersListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.adresIP,
            this.sessionID,
            this.status});
            this.usersListView.GridLines = true;
            this.usersListView.HideSelection = false;
            this.usersListView.Location = new System.Drawing.Point(33, 56);
            this.usersListView.Name = "usersListView";
            this.usersListView.Size = new System.Drawing.Size(632, 160);
            this.usersListView.TabIndex = 1;
            this.usersListView.UseCompatibleStateImageBehavior = false;
            this.usersListView.View = System.Windows.Forms.View.Details;
            this.usersListView.SelectedIndexChanged += new System.EventHandler(this.messagesListView_SelectedIndexChanged);
            // 
            // adresIP
            // 
            this.adresIP.Text = "IP maszyny";
            this.adresIP.Width = 100;
            // 
            // sessionID
            // 
            this.sessionID.Text = "Sesja";
            this.sessionID.Width = 115;
            // 
            // status
            // 
            this.status.Text = "Status";
            this.status.Width = 121;
            // 
            // portTextBox
            // 
            this.portTextBox.Location = new System.Drawing.Point(362, 20);
            this.portTextBox.Name = "portTextBox";
            this.portTextBox.Size = new System.Drawing.Size(100, 22);
            this.portTextBox.TabIndex = 2;
            this.portTextBox.Text = "13000";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(318, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(38, 17);
            this.label1.TabIndex = 3;
            this.label1.Text = "Port:";
            // 
            // startServerButton
            // 
            this.startServerButton.Location = new System.Drawing.Point(542, 12);
            this.startServerButton.Name = "startServerButton";
            this.startServerButton.Size = new System.Drawing.Size(123, 37);
            this.startServerButton.TabIndex = 4;
            this.startServerButton.Text = "Start Server";
            this.startServerButton.UseVisualStyleBackColor = true;
            this.startServerButton.Click += new System.EventHandler(this.startServerButton_Click);
            // 
            // messagesListView
            // 
            this.messagesListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.time,
            this.sessionIdentifier,
            this.direction,
            this.message});
            this.messagesListView.GridLines = true;
            this.messagesListView.HideSelection = false;
            this.messagesListView.Location = new System.Drawing.Point(33, 242);
            this.messagesListView.Name = "messagesListView";
            this.messagesListView.Size = new System.Drawing.Size(632, 196);
            this.messagesListView.TabIndex = 9;
            this.messagesListView.UseCompatibleStateImageBehavior = false;
            this.messagesListView.View = System.Windows.Forms.View.Details;
            // 
            // time
            // 
            this.time.Text = "Czas";
            this.time.Width = 71;
            // 
            // sessionIdentifier
            // 
            this.sessionIdentifier.Text = "Sesja";
            // 
            // direction
            // 
            this.direction.Text = "Kierunek";
            this.direction.Width = 79;
            // 
            // message
            // 
            this.message.Text = "Wiadomość";
            this.message.Width = 223;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(30, 445);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(272, 17);
            this.label2.TabIndex = 10;
            this.label2.Text = "Made By: Dominik Adamek, Maciej Chajda";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(706, 471);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.messagesListView);
            this.Controls.Add(this.startServerButton);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.portTextBox);
            this.Controls.Add(this.usersListView);
            this.Controls.Add(this.myLocalIP);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form1";
            this.Text = "Serwer";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label myLocalIP;
        private System.Windows.Forms.ListView usersListView;
        private System.Windows.Forms.ColumnHeader adresIP;
        private System.Windows.Forms.ColumnHeader sessionID;
        private System.Windows.Forms.ColumnHeader status;
        private System.Windows.Forms.TextBox portTextBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button startServerButton;
        private System.Windows.Forms.ListView messagesListView;
        private System.Windows.Forms.ColumnHeader time;
        private System.Windows.Forms.ColumnHeader direction;
        private System.Windows.Forms.ColumnHeader message;
        private System.Windows.Forms.ColumnHeader sessionIdentifier;
        private System.Windows.Forms.Label label2;
    }
}

