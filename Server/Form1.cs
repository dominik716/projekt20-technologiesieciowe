﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Server
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            myLocalIP.Text += Server.GetLocalIPAddress();
            Server.userListView = usersListView;
            Server.messagesListView = messagesListView;

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void startServerButton_Click(object sender, EventArgs e)
        {
            startServerButton.Enabled = false;
            startServerButton.Text = "Uruchomiony";
            portTextBox.Enabled = false;
            Thread t = new Thread(delegate ()
            {
                // replace the IP with your system IP Address...
                Server.Start(Server.GetLocalIPAddress(), int.Parse(portTextBox.Text));
            });
            t.Start();
        }

        private void messagesListView_SelectedIndexChanged(object sender, EventArgs e)
        {
            ListView zmienna = usersListView;
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }
    }
}
